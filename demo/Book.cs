﻿namespace LibraryManagement
{
    class Book
    {
        private int id;
        private string title;
        private string author;
        private bool available;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public bool Available
        {
            get { return available; }
            set { available = value; }
        }
    }
}
