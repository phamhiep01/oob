﻿using System;
using System.IO;

namespace LibraryManagement
{

    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library(5);

            while (true)
            {
                library.Menu();

                int choice;
                int id;

                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            library.PrintBookInfo();
                            break;
                        case 2:
                            Console.Write("Enter the code of the book to borrow: ");
                            id = Convert.ToInt32(Console.ReadLine());
                            library.CheckOut(id);
                            break;
                        case 3:
                            Console.Write("Enter the code of the book to return: ");
                            id = Convert.ToInt32(Console.ReadLine());
                            library.ReturnBook(id);
                            break;
                        case 4:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Invalid choice.");
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

            }


        }
    }
}