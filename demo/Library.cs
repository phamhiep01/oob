﻿namespace LibraryManagement
{
    class Library
    {
        private Book[] books;

        public Library(int size)
        {
            books = new Book[size];
            for (int i = 0; i < size; i++)
            {
                books[i] = new Book();
                books[i].Id = i + 1;
                books[i].Title = "Book " + (i + 1);
                books[i].Author = "Author " + (i + 1);
                books[i].Available = true;
            }
        }

        public void CheckOut(int id)
        {
            if (books[id - 1].Available)
            {
                Console.WriteLine("You have borrowed the book: " + books[id - 1].Title);
                books[id - 1].Available = false;

                // Write loan history to file
                using (StreamWriter sw = File.AppendText("loan_history.txt"))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "\t" + books[id - 1].Title + "\t" + "Borrowed");
                }
            }
            else
            {
                Console.WriteLine("The book is not available.");
            }
        }

        public void ReturnBook(int id)
        {
            if (!books[id - 1].Available)
            {
                Console.WriteLine("You have returned the book: " + books[id - 1].Title);
                books[id - 1].Available = true;

                // Write loan history to file
                using (StreamWriter sw = File.AppendText("loan_history.txt"))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "\t" + books[id - 1].Title + "\t" + "Returned");
                }
            }
            else
            {
                Console.WriteLine("The book is already available.");
            }
        }

        public void PrintBookInfo()
        {
            Console.WriteLine("BookID\t Title\tAuthor\t\tAvailable");
            for (int i = 0; i < books.Length; i++)
            {
                Console.WriteLine(books[i].Id + "\t" + books[i].Title + "\t" + books[i].Author + "\t" + books[i].Available);

                // Write book list to file
                using (StreamWriter sw = File.AppendText("book_list.txt"))
                {
                    sw.WriteLine(books[i].Id + "\t" + books[i].Title + "\t" + books[i].Author);
                    
                }
            }
        }
        
        public void Menu()
        {
            Console.WriteLine("\nMenu:");
            Console.WriteLine("1. View the list of books in the library.");
            Console.WriteLine("2. Borrow a book: Enter the code of the book to borrow.");
            Console.WriteLine("3. Return books: Enter the code of the book to return.");
            Console.WriteLine("4. Exit the program.");
        }
    }
}

    
